from screen import SnakeScr
from snake_model import Snake
from apple import Apple

apple = Apple() 
snake = Snake()
game = SnakeScr(snake, apple)
game.run()