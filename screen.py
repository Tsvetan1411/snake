import pygame
import snake_model
from sys import exit


class SnakeScr:
    def __init__(self, snake, apple):
        pygame.init()
        pygame.display.set_caption("SNAKE")
        self.screen = pygame.display.set_mode((700, 700))
        self.clock = pygame.time.Clock()
        self.snake = snake
        self.apple = apple
        self.game_speed = 2
        self.score = 0
        self.game_active = False
        self.first_play = True

    def disp_points(self):
        font = pygame.font.Font(None, size=30)
        score_text = f"Score: {self.score}"
        score_surf = font.render(score_text, False, (255, 255, 255))
        score_rect = score_surf.get_rect(topright=(680,0))
        self.screen.blit(score_surf, score_rect)

    def inc_speed(self):
        if self.score % 5 == 0:
            self.game_speed += 1

    def snake_col(self):
        pos_list = []
        for element in self.snake.snake_elements_list:
            pos_list.append(element.rect.center)
        if self.snake.snake_elements_list[0].rect.center in pos_list[1:]:
            self.game_active = False
        elif self.snake.snake_elements_list[0].rect.center[0] < 0 or self.snake.snake_elements_list[0].rect.center[0] > 700:
            self.game_active = False
        elif self.snake.snake_elements_list[0].rect.center[1] < 0 or self.snake.snake_elements_list[0].rect.center[1] > 700:
            self.game_active = False
            

    def menu(self):
        if self.first_play:
            font = pygame.font.Font(None, size=50)
            greet_text = "To start game press space"
            greet_surf = font.render(greet_text, False, (255, 255, 255))
            greet_rect = greet_surf.get_rect(center=(350, 350))
            self.screen.blit(greet_surf, greet_rect)

            key = pygame.key.get_pressed()
            if key[pygame.K_SPACE]:
                self.game_active = True
                self.first_play = False
        else:
            self.screen.fill("black")
            font = pygame.font.Font(None, size=50)
            greet_text = f"Score: {self.score}. To restart game press space"
            greet_surf = font.render(greet_text, False, (255, 255, 255))
            greet_rect = greet_surf.get_rect(center=(350, 350))
            self.screen.blit(greet_surf, greet_rect)

            key = pygame.key.get_pressed()
            if key[pygame.K_SPACE]:
                self.game_active = True
                self.score = 0
                self.game_speed = 2
                self.snake.reset()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()
                
                if event.type == pygame.KEYUP:
                    self.snake.change_dir(event)

            if self.game_active:
                self.screen.fill("black")
                self.snake.tail_movment()
                self.snake.head_movment()
                self.disp_points()

                if self.snake.rect.colliderect(self.apple.apple_rect):
                    self.snake.eat()
                    self.apple.apple_rect.center = (self.apple.rand_pos())
                    self.score += 1
                    self.inc_speed()

                self.snake_col()

                self.screen.blit(self.apple.apple_surf, self.apple.apple_rect)
                self.screen.blit(self.snake.head_surf, self.snake.rect)

                for x in range(1, len(self.snake.snake_elements_list)):
                    self.screen.blit(self.snake.snake_elements_list[x].tail_piece_surf, self.snake.snake_elements_list[x].rect)
            else:
                self.menu()
                
            pygame.display.update()
            self.clock.tick(self.game_speed)
