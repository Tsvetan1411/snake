import pygame
import random

class Apple:
    def __init__(self):
        self.apple_surf = pygame.Surface((20,20))
        self.apple_surf.fill("pink")
        self.pos = self.rand_pos()
        self.apple_rect = self.apple_surf.get_rect(center = self.pos)

    def rand_pos(self):
        x = random.randrange(10, 700, 20)
        y = random.randrange(10, 700, 20)
        return (x, y)
        