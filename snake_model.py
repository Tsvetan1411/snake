import pygame

class TailPiece:
    def __init__(self, pos):
        self.pos = pos
        self.tail_piece_surf = pygame.Surface((20,20))
        self.tail_piece_surf.fill("green")
        self.rect = self.tail_piece_surf.get_rect(center = self.pos)

class Snake:
    snake_elements_list = []
    def __init__(self):
        self.move_dir = "e"
        self.head_surf = pygame.Surface((20,20))
        self.head_surf.fill("red")
        self.rect = self.head_surf.get_rect(center = (390, 390))
        self.speed = 20
        self.snake_elements_list.append(self)
        self.snake_elements_list.append(TailPiece((370, 390)))
        self.snake_elements_list.append(TailPiece((350, 390)))

    def head_movment(self):
        if self.move_dir == "e":
            self.rect.x += self.speed
        elif self.move_dir == "w":
            self.rect.x -= self.speed
        elif self.move_dir == "s":
            self.rect.y += self.speed
        elif self.move_dir == "n":
            self.rect.y -= self.speed
    
    def change_dir(self, event):
        if event.key == pygame.K_UP and self.move_dir != "s":
            self.move_dir = "n"
        elif event.key == pygame.K_DOWN and self.move_dir != "n":
            self.move_dir = "s"
        elif event.key == pygame.K_LEFT and self.move_dir != "e":
            self.move_dir = "w"
        elif event.key == pygame.K_RIGHT and self.move_dir != "w":
            self.move_dir = "e"
        
    def eat(self):
        snake_length = len(self.snake_elements_list)
        last_pos = self.snake_elements_list[snake_length - 1].rect.center
        previous_pos = self.snake_elements_list[snake_length - 2].rect.center
        last_pos_x = last_pos[0]
        last_pos_y = last_pos[1]
        if previous_pos[0] == last_pos_x:
            if (previous_pos[1] - last_pos_y) < 0:
                self.snake_elements_list.append(TailPiece((last_pos_x, last_pos_y + 20)))
            else:
                self.snake_elements_list.append(TailPiece((last_pos_x, last_pos_y - 20)))
        elif previous_pos[1] == last_pos_y:
            if (previous_pos[0] - last_pos_x) < 0:
                self.snake_elements_list.append(TailPiece((last_pos_x + 20, last_pos_y)))
            else:
                self.snake_elements_list.append(TailPiece((last_pos_x - 20, last_pos_y)))

    def tail_movment(self):
        for x in reversed(range(1, len(self.snake_elements_list))):
            self.snake_elements_list[x].rect.center = self.snake_elements_list[x-1].rect.center

    def reset(self):
        del self.snake_elements_list[3:]
        self.snake_elements_list[0].rect.center = (390, 390)
        self.snake_elements_list[1].rect.center = (370, 390)
        self.snake_elements_list[2].rect.center = (350, 390)
        self.move_dir = "e"
        